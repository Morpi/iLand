$(document).ready(function(){
	
	// Start OwlCarousel
	$(".o_c_clients").owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    dots: false,
    smartSpeed: 1000,
    lazyLoad: true,
    responsive : {
      // breakpoint from 0 up
      0 : {
        items: 2,
      },
      // breakpoint from 480 up
      480 : {
        items: 3,
      },
      // breakpoint from 768 up
      768 : {
        items: 5,
      }
    }
  });

  $(".o_c_testimonials").owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    dots: false,
    smartSpeed: 1500
  });

	// scroll effect for navbar menu
	$(window).on('scroll', function(){
		if (Math.round($(window).scrollTop()) > 0) {
			$('.navbar').addClass('effect_navbar');
		}
		else {
			$('.navbar').removeClass('effect_navbar');
		}
	})

	// Activate WOW.js
	wow = new WOW( {
    boxClass:     'wow',
    animateClass: 'animated',
    offset:       0,
    mobile:       false,
    live:         true
  });
  wow.init();

	// Scroll to top
	$(window).scroll(function () {
    if ($(this).scrollTop() > 1000) {
      $('#back_top').fadeIn();
    } else {
      $('#back_top').fadeOut();
    }
  });

  // animate link back to top
  $('a#back_top').on('click', function(){
  	$('html, body').stop().animate({scrollTop:0}, 1000, "linear");
  })

  // animate links - anchor(#)
  $('a').on('click', function(){
  	var anchor = $(this);
  	$('html, body').stop().animate({
  		scrollTop: $(anchor.attr('href')).offset().top
  	}, 1500, 'linear')
  	event.preventDefault();
  })
});